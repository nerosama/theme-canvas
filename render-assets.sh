#!/bin/bash

if [ ! "$(which inkscape 2> /dev/null)" ]; then
  echo inkscape needs to be installed to generate the png.
  exit 1
fi

if [ ! "$(which optipng 2> /dev/null)" ]; then
  echo optipng needs to be installed to generate the png.
  exit 1
fi

# Variables
INKSCAPE="/usr/bin/inkscape"
OPTIPNG="/usr/bin/optipng"

# Thumbnail Function
function thumbnail() {
  THUMB_FILE="src/gtk-3.0/thumbnail.svg"
  THUMB_DIR="src/gtk-3.0"

  THUMB_INDEX=('thumbnail' 'thumbnail-dark')

  for i in "${THUMB_INDEX[@]}"; do
    if [ -f $THUMB_DIR/$i.png ]; then
      echo Deleting $THUMB_DIR/$i.png
      rm $THUMB_DIR/$i.png
    fi

    echo
    echo Rendering $THUMB_DIR/$i.png
    $INKSCAPE --export-id=$i \
              --export-id-only \
              --export-png=$THUMB_DIR/$i.png $THUMB_FILE >/dev/null \
    && $OPTIPNG -o7 --quiet $THUMB_DIR/$i.png 
  done
}

# GTK2 Function
function gtk2() {
  cd src/gtk-2.0
  rm assets/*.png
  rm assets-dark/*.png
  ./render-assets.sh
}

# GTK3 Function
function gtk3() {
  cd src/gtk-3.0
  rm assets/*.png
  ./render-assets.sh
}

# XFWM Function
function xfwm() {
  cd src/xfwm4
  rm assets/*.png
  ./render-assets.sh
}

case "${1}" in
  "")
    thumbnail
    gtk2
    gtk3
    xfwm
    ;;
  gtk)
    thumbnail
    gtk2
    gtk3
    ;;
  gtk2)
    gtk2
    ;;
  gtk3)
    gtk3
    ;;
  xfwm)
    xfwm
    ;;
  *)
    echo "Unknown argument: '${1}'"
    echo "Use 'gtk', 'gtk2', 'gtk3' or 'xfwm' as an argument."
    ;;
esac
