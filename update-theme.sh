#!/bin/bash

THEME_NAME="Machina"
THEME_FILE="machina-gtk-theme"
THEME_REPO="https://github.com/vinceliuice/vimix-gtk-themes/archive/master.zip"

notify-send "${THEME_NAME}" "Getting the latest version of the ${THEME_NAME} themes..." -i system-software-update; 
cd /tmp/; 
rm -Rf /tmp/${THEME_FILE}.zip 2>/dev/null; 
rm -Rf /tmp/${THEME_FILE}-master/ 2>/dev/null; 
wget ${THEME_REPO} -O ${THEME_FILE}.zip; 
unzip ${THEME_FILE}.zip; 
cd ${THEME_FILE}-master; 
./Install; 
notify-send "All done!" "Enjoy the latest version of ${THEME_NAME}!" -i face-smile
